# accounts/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('signup', views.signup, name='signup'),
    path('profile', views.view_profile, name='view_profile'),
    path('profile/edit/', views.edit_profile, name='edit_profile'),
    path('profile/password/', views.change_password, name='change_password'),
]
