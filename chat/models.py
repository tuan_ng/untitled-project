from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone

User = get_user_model()

class Room(models.Model):
    title = models.CharField(max_length=256)
    
    def __str__(self):
        return self.title
        
class Message(models.Model):
    author = models.ForeignKey(User, related_name='author_message', on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField()
    room = models.ForeignKey(Room, related_name='room_message', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.author.username

    def last_20_messages():
        return Message.objects.order_by('timestamp').all()[:20]
