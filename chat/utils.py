from .models import Room

def get_room_or_creat(room_name):
    """
    Tries to get a room id by room_name
    Otherwise, create a new one
    """
    try:
        room = Room.objects.filter(title=room_name)[0]
    except:
        room = Room.objects.create(title=room_name)
    
    return room.id
