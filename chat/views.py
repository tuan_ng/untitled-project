from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
import json
from .models import Room
# Create your views here.
def index(request):
    rooms = Room.objects.order_by("title")[:20]
    if request.GET:
        if request.GET['sort'] == 'name':
            pass
        elif request.GET['sort'] == 'message':
            rooms = sorted(list(rooms), key=lambda x: x.room_message.count(), reverse=True)
    
    return render(request, 'chat/index.html', {
        "rooms": rooms,
    })

@login_required
def room(request, room_name):
    return render(request, 'chat/room.html', {
        'room_name': room_name,
        'room_name_json': mark_safe(json.dumps(room_name)),
        'user_name_json': mark_safe(json.dumps(request.user.username))
    })
